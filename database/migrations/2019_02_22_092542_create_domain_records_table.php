<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_records', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('domain_id');
            $table->string('sub_domain');
            $table->string('type');
            $table->string('value');

            $table->foreign('domain_id')->references('id')->on('domains');

            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_records');
    }
}
