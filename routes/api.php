<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'LoginController@login');
        Route::post('register', 'RegisterController@register');

        Route::get('refresh', 'LoginController@refresh')->middleware('jwt.refresh');
        Route::get('user', 'LoginController@user')->middleware('jwt.auth');
        Route::post('logout', 'LoginController@logout')->middleware('jwt.auth');
    });

    Route::apiResource('domain', 'DomainController');

    Route::get('domain/{domain}/record', 'RecordController@index')->middleware('jwt.auth');
    Route::post('domain/{domain}/record/import', 'RecordController@import')->middleware('jwt.auth');
    Route::get('domain/{domain}/record/export', 'RecordController@export')->middleware('jwt.auth');
})
;
