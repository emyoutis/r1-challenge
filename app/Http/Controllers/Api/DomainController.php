<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Domain\DomainStoreRequest;
use App\Http\Requests\Api\Domain\DomainUpdateRequest;
use App\Models\Domain;
use Exception;

class DomainController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = Domain::orderByDesc('created_at')->paginate(20);

        return response()->json($paginator);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param DomainStoreRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(DomainStoreRequest $request, Domain $domain)
    {
        $domain
             ->fill($request->all())
             ->save()
        ;

        return response()->json($domain->toArray());
    }



    /**
     * Display the specified resource.
     *
     * @param Domain $domain
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Domain $domain)
    {
        return response()->json($domain->toArray());
    }



    /**
     * Update the specified resource in storage.
     *
     * @param DomainUpdateRequest $request
     * @param Domain              $domain
     *
     * @return \Illuminate\Http\Response
     */
    public function update(DomainUpdateRequest $request, Domain $domain)
    {
        $domain->update($request->all());

        return response()->json($domain->toArray());
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param Domain $domain
     *
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy(Domain $domain)
    {
        return response()->json([
             'deleted' => $domain->delete(),
        ]);
    }
}
