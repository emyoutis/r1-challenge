<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegisterController extends Controller
{
    /**
     * Registers a user with the given data.
     *
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $this->create($request->all());

        return response()->json($this->generateResult($request));
    }



    /**
     * Creates a user based on the given data.
     *
     * @param array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
             $this->usernameField() => $data[$this->usernameField()],
             'name'                 => $data['name'],
             'password'             => Hash::make($data['password']),
        ]);
    }



    /**
     * Returns the username field.
     *
     * @return string
     */
    protected function usernameField()
    {
        return User::usernameField();
    }



    /**
     * Generates and returns the result array.
     *
     * @param RegisterRequest $request
     *
     * @return array
     */
    protected function generateResult(RegisterRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $token       = JWTAuth::attempt($credentials);

        return [
             "access_token" => $token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ];
    }
}
