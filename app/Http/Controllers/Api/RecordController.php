<?php

namespace App\Http\Controllers\Api;

use App\Exports\RecordsExport;
use App\Http\Requests\Api\Record\RecordImportRequest;
use App\Imports\RecordsImport;
use App\Models\Domain;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class RecordController extends Controller
{
    /**
     * Returns the list of records based on the specified domains.
     *
     * @param Domain $domain
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Domain $domain)
    {
        $paginator = $domain
             ->records()
             ->orderByDesc('created_at')
             ->paginate(20)
        ;

        return response()->json($paginator);
    }



    /**
     * Imports an Excel file to the `domain_records` table.
     *
     * @param RecordImportRequest $request
     * @param Domain              $domain
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function import(RecordImportRequest $request, Domain $domain)
    {
        $file = $request->file('file');
        Excel::import(
             new RecordsImport($domain),
             $file->getRealPath(),
             null, ucfirst
             ($file->extension())
        );

        return response()->json([
             'imported' => true,
        ]);
    }



    /**
     * Serves an Excel file containing the records of the specified domain.
     *
     * @param Domain $domain
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Domain $domain)
    {
        return Excel::download(new RecordsExport($domain), "domain-$domain->id-records.xlsx");
    }
}
