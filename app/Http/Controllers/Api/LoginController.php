<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    /**
     * Attempts to login a user with the given credentials.
     *
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json([
             "access_token" => $token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ])->header('Authorization', $token)
             ;
    }



    /**
     * Renders the logged in user.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function user()
    {
        $user = Auth::user();

        return response([
             'status' => 'success',
             'data'   => $user,
        ]);
    }



    /**
     * Refreshes token.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function refresh()
    {
        return response([
             'status' => 'success',
        ]);
    }



    /**
     * Invalidates token.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function logout()
    {
        JWTAuth::invalidate();

        return response([
             'status' => 'success',
             'msg'    => 'Logged out Successfully.',
        ], 200);
    }
}
