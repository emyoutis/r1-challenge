<?php

namespace App\Http\Requests\Api\Domain;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DomainUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'address' => [
                  'regex:/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/',
                  Rule::unique('domains', 'address')
                      ->ignore($this->route()->parameter('domain')->id),
             ],
             'serial_number' => 'int|min:1',
             'refresh'       => 'int|min:1',
             'retry'         => 'int|min:1',
             'expire'        => 'int|min:1',
             'default_ttl'   => 'int|min:1',
        ];
    }
}
