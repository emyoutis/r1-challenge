<?php

namespace App\Http\Requests\Api\Domain;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DomainStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'address'       => [
                  'required',
                  'regex:/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/',
                  Rule::unique('domains', 'address'),
             ],
             'serial_number' => 'required|int|min:1',
             'refresh'       => 'required|int|min:1',
             'retry'         => 'required|int|min:1',
             'expire'        => 'required|int|min:1',
             'default_ttl'   => 'required|int|min:1',
        ];
    }
}
