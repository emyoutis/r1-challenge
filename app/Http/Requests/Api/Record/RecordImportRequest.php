<?php

namespace App\Http\Requests\Api\Record;

use Illuminate\Foundation\Http\FormRequest;

class RecordImportRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'file' => [
                  'required',
                  'file',
                  'mimes:xlsx,xls',
             ],
        ];
    }
}
