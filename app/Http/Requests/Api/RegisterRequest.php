<?php

namespace App\Http\Requests\Api;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guest();
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             $this->username() => [
                  'required',
                  'string',
                  Rule::unique('users', $this->username()),
             ],
             'name'            => 'required|string',
             'password'        => 'required|string|min:8|max:32|confirmed',
        ];
    }



    /**
     * Returns the username field.
     *
     * @return string
     */
    protected function username()
    {
        return User::usernameField();
    }
}
