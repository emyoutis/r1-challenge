<?php

namespace App\Imports;

use App\Models\Domain;
use Maatwebsite\Excel\Concerns\ToModel;

class RecordsImport implements ToModel
{
    /** @var Domain */
    protected $domain;



    /**
     * RecordsImport constructor.
     *
     * @param Domain $domain_id
     */
    public function __construct(Domain $domain_id)
    {
        $this->domain = $domain_id;
    }



    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return $this->domain->records()->make([
             'sub_domain' => $row[0],
             'type'       => $row[1],
             'value'      => $row[2],
        ])
             ;
    }
}
