<?php

namespace App\Exports;

use App\Models\Domain;
use App\Models\DomainRecord;
use Maatwebsite\Excel\Concerns\FromCollection;

class RecordsExport implements FromCollection
{
    /** @var Domain */
    protected $domain;



    /**
     * RecordsImport constructor.
     *
     * @param Domain $domain_id
     */
    public function __construct(Domain $domain_id)
    {
        $this->domain = $domain_id;
    }



    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this
             ->domain
             ->records
             ->map(function (DomainRecord $record) {
                 return [
                      $record->sub_domain,
                      $record->type,
                      $record->value,
                 ];
             });
    }
}
