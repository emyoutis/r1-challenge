<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/21/19
 * Time: 5:13 PM
 */

namespace App\Models\Traits;


trait UserAuthTrait
{
    /**
     * Returns the username field of the user.
     *
     * @return string
     */
    public static function usernameField()
    {
        return 'email';
    }
}
