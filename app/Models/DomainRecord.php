<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DomainRecord extends Model
{
    public $fillable = ['sub_domain', 'type', 'value'];



    /**
     * Relationship with Domains
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }
}
