<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
         'address',
         'serial_number',
         'refresh',
         'retry',
         'expire',
         'default_ttl',
    ];



    /**
     * Relationship with Records
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function records()
    {
        return $this->hasMany(DomainRecord::class);
    }
}
