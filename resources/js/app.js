import Vue from 'vue';

import VueRouter from 'vue-router';

import axios from 'axios';

import VueAxios from 'vue-axios';

import BootstrapVue from 'bootstrap-vue'


import App from './App.vue';

import Dashboard from './views/Dashboard.vue';

import DomainForm from './views/DomainForm.vue';

import Records from './views/Records.vue';

import RecordImport from './views/RecordImport.vue';

import Home from './views/Home.vue';

import Register from './views/Register.vue';

import Login from './views/Login.vue';

Vue.use(VueRouter);

Vue.use(VueAxios, axios);

Vue.use(BootstrapVue);

axios.defaults.baseURL = '/api';

const router = new VueRouter({

    routes: [
        {

            path: '/',

            name: 'home',

            component: Home

        }, {

            path: '/register',

            name: 'register',

            component: Register,

            meta: {

                auth: false
            }

        }, {

            path: '/login',

            name: 'login',

            component: Login,

            meta: {

                auth: false
            }
        }, {

            path: '/dashboard',

            name: 'dashboard',

            component: Dashboard,

            meta: {

                auth: true

            }

        }, {

            path: '/dashboard/domain/new',

            name: 'new_domain',

            component: DomainForm,

            meta: {

                auth: true

            }

        }, {

            path: '/dashboard/domain/:id',

            name: 'domain-single',

            component: DomainForm,

            meta: {

                auth: true

            }

        } , {

            path: '/dashboard/domain/:id/records',

            name: 'domain_records',

            component: Records,

            meta: {

                auth: true

            }
        }, {

            path: '/dashboard/domain/:id/records/import',

            name: 'domain_records_import',

            component: RecordImport,

            meta: {

                auth: true

            }
        }
    ]

});

Vue.router = router

Vue.use(require('@websanova/vue-auth'), {

    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),

    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),

    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),

});

App.router = Vue.router

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

new Vue(App).$mount('#app');
